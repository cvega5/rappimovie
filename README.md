RappiMovie
==========

Este es el README de la aplicacion de prueba para rappi.

Capas de la aplicaci�n
----------------------
- Uso el patr�n MVC (Modelo, vista, Controlador), en Modelo se encuentra el objeto Movie el cual se encarga de mapear
los atributos que necesitemos usar en nuestra aplicaci�n.

- En el Controlador se encuentran MainActivity, Detalle_pelicula que son los encargados de comunicar el modelo con
las vistas. MainActivity es la vista principal donde se cargan los listados de peliculas anteriormente mencionados
(Popular, Top rated y Upcoming. Detalle_pelicula es el encargado de mostrar la informaci�n de la pelicula seleccionada
usando el modelo Movie.

- Adapters son unas clases encargadas de pasar la informaci�n que viene del Api y transformarla en items que son
enviados al MainActivity. Dentro de los adapters se encuentra PopularAdapter que es la clase encargada de crear los 
modelos de peliculas populares y pasarlos al controlador MainActivity.
Tambien se encuentra TopRatedAdapter que es la clase encargada de crear los modelos de peliculas Top y pasarlos 
al controlador MainActivity, De igual forma se encuentra UpcomingAdapter que es la clase encargada de crear los 
modelos de peliculas Upcoming y pasarlos al controlador MainActivity.

- Hay una clase llamada MyVolleySingleton esta clase es la encargada de hacer la cola de peticiones que se hacen al
API, tambien es la encargada de almacenar en cach� informaci�n correspondiente al listado de peliculas y algunas
imagenes.

- Las vistas son las encargadas de mostrar la informaci�n que es pasada por los controladores, estas vistas se encuentran
dentro de la carpeta res/layout aqui es donde se definen los elementos que conforman la UI (interfaz de usuario).


En qu� consiste el principio de responsabilidad �nica? Cu�l es su prop�sito?
============================================================================

El principio de responsabilidad unica consiste en que cada clase debe ser responsable de una sola funcionalidad
dentro de la aplicaci�n, esto ayuda a que las aplicaciones no sean tan sensibles al cambio.

Qu� caracter�sticas tiene, seg�n su opini�n, un �buen� c�digo o c�digo limpio?
==============================================================================

En mi opinion un buen codigo es aquel que se puede leer con facilidad, de facil comprensi�n, es claro con 
las variables y tiene un orden. En ocasiones tiene comentarios para explicar una que otra funcionalidad.


Link descarga APK
-----------------

https://drive.google.com/open?id=1pR9t4oxe7sWI2CPuPAj0Yb_R9gQUGqcp

