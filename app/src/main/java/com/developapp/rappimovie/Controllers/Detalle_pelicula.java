package com.developapp.rappimovie.Controllers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.developapp.rappimovie.Helpers.MyVolleySingleton;
import com.developapp.rappimovie.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Detalle_pelicula extends AppCompatActivity implements YouTubePlayer.OnInitializedListener {

    private RequestQueue queue;
    private ImageLoader imageLoader;
    private TextView movie_title, movie_rating, movie_description, release_date, video;
    private ImageView imagebackdrop;
    private YouTubePlayerFragment playerFragment;
    private YouTubePlayer mPlayer;
    private String key_video = "";
    private int id;
    private LinearLayout linear_youtube_fragment;
    private static final String TAG = "Detalle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pelicula);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = MyVolleySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
        imageLoader = MyVolleySingleton.getInstance(getApplicationContext()).getImageLoader();

        imagebackdrop = findViewById(R.id.image_backdrop);
        movie_description = findViewById(R.id.movie_description);
        movie_rating = findViewById(R.id.movie_rating);
        movie_title = findViewById(R.id.movie_title);
        release_date = findViewById(R.id.release_date);
        video = findViewById(R.id.label_video);
        linear_youtube_fragment = findViewById(R.id.linear_youtube_fragment);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            id = bundle.getInt("id");
            imageLoader.get(getString(R.string.url_imagen_detalle_backdrop) + bundle.getString("image_back"),
                    ImageLoader.getImageListener(imagebackdrop, R.drawable.ic_sync, R.drawable.ic_error));
            movie_title.setText(bundle.getString("title"));
            movie_rating.setText(String.valueOf(bundle.getDouble("average")));
            movie_description.setText(bundle.getString("overview"));
            release_date.setText("Release date: " + bundle.getString("date"));

            getMoviesVideos();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        mPlayer = youTubePlayer;

        //Enables automatic control of orientation
        mPlayer.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);

        //Show full screen in landscape mode always
        mPlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);

        //System controls will appear automatically
        mPlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);

        if (!b) {
            mPlayer.cueVideo(key_video);
            //mPlayer.loadVideo("9rLZYyMbJic");
        } else {
            mPlayer.play();
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        mPlayer = null;
    }

    private void getMoviesVideos() {
        String url = getString(R.string.url_lista_videos) + id + getString(R.string.url_lista_videos_2);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getJSONArray("results").length() > 0) {
                                JSONArray jsonArray = response.getJSONArray("results");
                                if (jsonArray.length() > 0) {
                                    key_video = jsonArray.getJSONObject(0).getString("key");
                                    video.setVisibility(View.VISIBLE);
                                    linear_youtube_fragment.setVisibility(View.VISIBLE);
                                    playerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_player_fragment);
                                    playerFragment.initialize(getString(R.string.youtube_api_key), Detalle_pelicula.this);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            video.setVisibility(View.GONE);
                            linear_youtube_fragment.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_lista_videos", error.getMessage());
                    }
                });
        request.setTag(TAG);
        MyVolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }


}
