package com.developapp.rappimovie.Controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.developapp.rappimovie.Adapters.PopularAdapter;
import com.developapp.rappimovie.Adapters.TopRatedAdapter;
import com.developapp.rappimovie.Adapters.UpcomingAdapter;
import com.developapp.rappimovie.Helpers.MyVolleySingleton;
import com.developapp.rappimovie.R;

public class MainActivity extends AppCompatActivity {

    private RequestQueue queue;
    public static final String TAG = "Main";
    private ListView movies_list;
    private ArrayAdapter adapter, adapter_topRated, adapter_upcoming;
    private SearchView searchView;
    private String SIGNAL;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    adapter = new PopularAdapter(getApplicationContext(), MainActivity.this);
                    movies_list.setAdapter(adapter);
                    SIGNAL = "popular";
                    return true;
                case R.id.navigation_dashboard:
                    adapter_topRated = new TopRatedAdapter(getApplicationContext(), MainActivity.this);
                    movies_list.setAdapter(adapter_topRated);
                    SIGNAL = "top";
                    return true;
                case R.id.navigation_notifications:
                    adapter_upcoming = new UpcomingAdapter(getApplicationContext(), MainActivity.this);
                    movies_list.setAdapter(adapter_upcoming);
                    SIGNAL = "upcoming";
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        queue = MyVolleySingleton.getInstance(this.getApplicationContext()).getRequestQueue();

        movies_list = findViewById(R.id.lista_peliculas);
        adapter = new PopularAdapter(this, MainActivity.this);
        adapter_topRated = new TopRatedAdapter(this, MainActivity.this);
        adapter_upcoming = new UpcomingAdapter(this, MainActivity.this);
        movies_list.setAdapter(adapter);

        SIGNAL = "popular";
        searchView = findViewById(R.id.search_bar_menu);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                switch (SIGNAL){
                    case "popular":
                        if(!query.trim().equals("")){
                            adapter = new PopularAdapter(getApplicationContext(), query, MainActivity.this);
                            movies_list.setAdapter(adapter);
                        }
                        break;
                    case "top":
                        if(!query.trim().equals("")){
                            adapter_topRated = new TopRatedAdapter(getApplicationContext(), query, MainActivity.this);
                            movies_list.setAdapter(adapter_topRated);
                        }
                        break;
                    case "upcoming":
                        if(!query.trim().equals("")){
                            adapter_upcoming = new UpcomingAdapter(getApplicationContext(), query, MainActivity.this);
                            movies_list.setAdapter(adapter_upcoming);
                        }
                        break;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                switch (SIGNAL){
                    case "popular":
                        if(newText.trim().equals("")){
                            adapter = new PopularAdapter(getApplicationContext(), MainActivity.this);
                            movies_list.setAdapter(adapter);
                        }
                        break;
                    case "top":
                        if(newText.trim().equals("")){
                            adapter_topRated = new TopRatedAdapter(getApplicationContext(), MainActivity.this);
                            movies_list.setAdapter(adapter_topRated);
                        }
                        break;
                    case "upcoming":
                        if(newText.trim().equals("")){
                            adapter_upcoming = new UpcomingAdapter(getApplicationContext(), MainActivity.this);
                            movies_list.setAdapter(adapter_upcoming);
                        }
                        break;
                }
                return false;
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }
}
