package com.developapp.rappimovie.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.developapp.rappimovie.Controllers.Detalle_pelicula;
import com.developapp.rappimovie.Helpers.MyVolleySingleton;
import com.developapp.rappimovie.Models.Movie;
import com.developapp.rappimovie.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UpcomingAdapter extends ArrayAdapter{

    private static final String TAG = "Main";
    private ArrayList<Movie> Movies;
    private ImageLoader imageLoader;
    private Activity _Activity;


    public UpcomingAdapter(@NonNull Context context, Activity activity) {
        super(context, 0);
        imageLoader = MyVolleySingleton.getInstance(context.getApplicationContext()).getImageLoader();
        _Activity = activity;
        getUpcomingMovies();
    }

    public UpcomingAdapter(@NonNull Context context, String query, Activity activity) {
        super(context, 0);
        imageLoader = MyVolleySingleton.getInstance(context.getApplicationContext()).getImageLoader();
        _Activity = activity;
        getUpcomingMovies(query);
    }

    @Override
    public int getCount() {
        return Movies != null ? Movies.size() : 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View itemView;

        itemView = null == convertView ? inflater.inflate(R.layout.item_movie,
                parent,
                false) : convertView;

        final Movie item = Movies.get(position);

        final TextView movie_name = itemView.findViewById(R.id.movie_name);
        final ImageView image_movie = itemView.findViewById(R.id.image_post);

        movie_name.setText(item.getTitle());

        imageLoader.get("http://image.tmdb.org/t/p/w92"+item.getImagen_poster_path(),
                ImageLoader.getImageListener(image_movie, R.drawable.ic_sync, R.drawable.ic_error));

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getContext().getApplicationContext(), Detalle_pelicula.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", item.getId());
                bundle.putDouble("average", item.getAverage());
                bundle.putString("title", item.getTitle());
                bundle.putString("image_back", item.getImagen_backdrop());
                bundle.putString("overview", item.getOverview());
                bundle.putString("date", item.getRelease_date());
                intent.putExtras(bundle);
                _Activity.startActivity(intent);
            }
        });

        return itemView;
    }

    private void getUpcomingMovies() {
        String url = "https://api.themoviedb.org/3/movie/upcoming?api_key=6a621164c06dbb50c0d27b76fd439c93&language=en-US&page=1";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Movies = parseJson(response);
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_upcoming_movies", error.getMessage());
                    }
                });
        request.setTag(TAG);
        MyVolleySingleton.getInstance(getContext().getApplicationContext()).addToRequestQueue(request);
    }

    private void getUpcomingMovies(final String query) {
        String url = "https://api.themoviedb.org/3/movie/upcoming?api_key=6a621164c06dbb50c0d27b76fd439c93&language=en-US&page=1";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Movies = parseJson(response, query);
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error_upcoming_movies", error.getMessage());
                    }
                });
        request.setTag(TAG);
        MyVolleySingleton.getInstance(getContext().getApplicationContext()).addToRequestQueue(request);
    }

    private ArrayList<Movie> parseJson(JSONObject jsonObject){
        ArrayList<Movie> movies = new ArrayList<>();
        JSONArray jsonArray  = null;

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                Movie movie = new Movie(
                        object.getInt("id"),
                        object.getDouble("vote_average"),
                        object.getString("title"),
                        object.getString("backdrop_path"),
                        object.getString("overview"),
                        object.getString("poster_path"),
                        object.getString("release_date")
                );

                movies.add(movie);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return movies;
    }

    private ArrayList<Movie> parseJson(JSONObject jsonObject, String query){
        ArrayList<Movie> movies = new ArrayList<>();
        JSONArray jsonArray  = null;

        try {
            jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                if (object.getString("title").toLowerCase().contains(query.toLowerCase())) {
                    Movie movie = new Movie(
                            object.getInt("id"),
                            object.getDouble("vote_average"),
                            object.getString("title"),
                            object.getString("backdrop_path"),
                            object.getString("overview"),
                            object.getString("poster_path"),
                            object.getString("release_date")
                    );
                    movies.add(movie);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return movies;
    }

}
