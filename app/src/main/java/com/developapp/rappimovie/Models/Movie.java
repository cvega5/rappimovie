package com.developapp.rappimovie.Models;

public class Movie {

    private Integer id;
    private Double average;
    private String title;
    private String imagen_backdrop;
    private String overview;
    private String imagen_poster_path;
    private String release_date;

    public Movie(Integer id, Double average, String title, String imagen_backdrop, String overview,
                 String imagen_poster_path, String release_date) {
        this.id = id;
        this.average = average;
        this.title = title;
        this.imagen_backdrop = imagen_backdrop;
        this.overview = overview;
        this.imagen_poster_path = imagen_poster_path;
        this.release_date = release_date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagen_backdrop() {
        return imagen_backdrop;
    }

    public void setImagen_backdrop(String imagen_backdrop) {
        this.imagen_backdrop = imagen_backdrop;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getImagen_poster_path() {
        return imagen_poster_path;
    }

    public void setImagen_poster_path(String imagen_poster_path) {
        this.imagen_poster_path = imagen_poster_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

}
